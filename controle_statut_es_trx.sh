#!/bin/sh


cd $HOME/app/bin

rm liste_es_zero.txt liste_es_inactif.txt  liste_es_actif.txt

DATFIL=`date -d -1day +%Y%m%d`

mkdir -p /backup/rapport/bp/$DATFIL

REP=/backup/rapport/bp/$DATFIL

JOURS_POUR_SUSPENSION=120

psql dbtransactions botransactions  << fininstruct


-- # la liste des espaces service ayant des encaissements avec type A , Z et I #--


CREATE temp table tmp1 as
SELECT es_code,
       case
           WHEN (max(to_char(to_timestamp(date_transaction, 'YYYYMMDDHH24MISS'), 'yyyy-mm-dd'))::date >=
                 to_char((current_date - interval '1day'), 'YYYY-MM-DD')::date) THEN 'A'

           WHEN (max(to_char(to_timestamp(date_transaction, 'YYYYMMDDHH24MISS'), 'yyyy-mm-dd'))::date <
                 to_char((current_date - interval '1day'), 'YYYY-MM-DD')::date and
                 max(to_char(to_timestamp(date_transaction, 'YYYYMMDDHH24MISS'), 'yyyy-mm-dd'))::date >=
                 to_char((current_date - interval '30day'), 'YYYY-MM-DD')::date) THEN 'Z'

           WHEN (max(to_char(to_timestamp(date_transaction, 'YYYYMMDDHH24MISS'), 'yyyy-mm-dd'))::date <
                 to_char((current_date - interval '${JOURS_POUR_SUSPENSION}day'), 'YYYY-MM-DD')::date) THEN 'S'

           else 'I' END
FROM espace_service,
     transactions
WHERE (es_statut = 'Z' or es_statut = 'A' or es_statut = 'I')
  and es_code not between '004601' and '004613'
  and es_code not between '005601' and '005604'
  and es_code not in ('012005', '007100', '007101')
  and es_code not between '005401' and '005450'
  and es_code not between '005501' and '005521'
  and es_code not between '005601' and '005604'
  and es_code not between '020001' and '020004'
  and es_code not between '030000' and '030015'
  and es_code not between '100000' and '100054'
  and es_code not between '000975' and '000976'
  and es_code not in
      ('050000', '200000', '007009', '001000', '000813', '000800', '011002', '011001', '010005', '010999')
  and gtrx_esp = es_code
  and (es_statut = 'Z' or es_statut = 'A' or es_statut = 'I')
  and gtrx_etat = 'T'
  and (txn_type = 'M' or txn_type = 'R' or txn_type = 'E' or txn_type = 'H' or txn_type = 'F' or txn_type = 'S' or
       txn_type = 'D' or txn_type = 'R' or txn_type = 'C')
group by 1;


------------------------------------------------------------------------------------------------------------------------------------

-- # liste ESs statut S #--

\copy (SELECT es_code,"case" FROM tmp1  WHERE "case" ='S') to 'liste_es_suspendu.txt' delimiter as ';'

-- # liste ESs statut Z #--

\copy (SELECT es_code,"case" FROM tmp1  WHERE "case" ='Z') to 'liste_es_zero.txt' delimiter as ';'

-- # liste ESs statut I#--

\copy (SELECT es_code,"case" FROM tmp1  WHERE "case" ='I') to 'liste_es_inactif.txt' delimiter as ';'


-- # liste ESs statut A #--

\copy (SELECT es_code,"case" FROM tmp1  WHERE "case" ='A') to 'liste_es_actif.txt' delimiter as ';'


\q

fininstruct

cp liste_es_suspendu.txt $REP/liste_es_suspendu.csv
cp liste_es_zero.txt $REP/liste_es_zero.csv
cp liste_es_inactif.txt $REP/liste_es_inactif.csv
cp liste_es_actif.txt $REP/liste_es_actif.csv

sed -i '1i \Code\ ES\;Statut\ ES' $REP/liste_es_suspendu.csv
sed -i '1i \Code\ ES\;Statut\ ES' $REP/liste_es_zero.csv
sed -i '1i \Code\ ES\;Statut\ ES' $REP/liste_es_inactif.csv
sed -i '1i \Code\ ES\;Statut\ ES' $REP/liste_es_actif.csv

