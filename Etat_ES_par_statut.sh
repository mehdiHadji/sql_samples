#!/bin/sh
REP=/backup/rapport/backoffice/rapport

mkdir -p $REP/`date -d -1day +%Y%m%d`
cd $REP/`date -d -1day +%Y%m%d`

psql dbbackoffice userbackoffice <<fin_instruct

CREATE TEMP table sector AS SELECT es_code code1, es_nomactiv sect FROM espace_service LEFT OUTER JOIN sectactiv ON es_sectact = codeactiv;
CREATE TEMP table region AS SELECT es_code code2, reg_nom reg FROM espace_service LEFT OUTER JOIN boregion  ON es_region  = reg_code;
CREATE TEMP table mag AS SELECT es_code code3, type_magasin mag FROM espace_service LEFT OUTER JOIN typemagasin ON es_typmag = code_magasin ;
CREATE TEMP table globale AS SELECT code1, sect, reg, mag FROM sector, region, mag WHERE code1=code2 AND code1=code3 ORDER BY 1;


\COPY (SELECT es_code, es_nom, es_statut, sect, es_adr, es_quartier, es_ville, reg, es_gerant, to_char(to_timestamp(es_dateder, 'YYYYMMDDHH24MISS'),'dd/mm/yyyy'), to_char(to_timestamp(es_datetrx, 'YYYYMMDDHH24MISS'),'dd/mm/yyyy') FROM espace_service, globale WHERE es_code=code1 ORDER BY es_code) to 'Liste_ES_Globale_`date -d -1day +%d-%m-%Y`.csv' delimiter as ';'
\COPY (SELECT es_code, es_nom, es_statut, sect, es_adr, es_quartier, es_ville, reg, es_gerant, to_char(to_timestamp(es_dateder, 'YYYYMMDDHH24MISS'),'dd/mm/yyyy'), to_char(to_timestamp(es_datetrx, 'YYYYMMDDHH24MISS'),'dd/mm/yyyy') FROM espace_service, globale WHERE es_code=code1 AND es_statut in ('A','S','I','C','Z') AND es_nom not ilike 'libre%' order by es_code ) to 'Liste_ES_ASIC_`date -d -1day +%d-%m-%Y`.csv' delimiter as ';'
\COPY (select es_code, '', '', es_adr, es_quartier, es_ville, es_telf, es_telm, sect FROM espace_service, globale WHERE es_code=code1  and es_statut in ('A','S','Z') and es_nom not ilike 'libre%' and es_observ <>'HR' order by 1 ) to 'ES_Liste_WEB_TS_`date -d -1day +%d-%m-%Y`.csv' delimiter as ';'
\COPY (select es_code, '', '', es_adr, es_quartier, es_ville, es_telf, es_telm, sect FROM espace_service, globale WHERE es_code=code1  and es_statut in ('A','S','Z') and es_nom not ilike 'libre%' and es_observ <>'HR' order by 1 ) to 'ES_Liste_WEB_CC_`date -d -1day +%d-%m-%Y`.csv' delimiter as ';'
\q
fin_instruct

sed -i "1i Code ES;Nom ES;Statut;Activite;Adresse;Quartier;Ville;Zone;Gerant;date der;date trx"  Liste_ES_*

sed -i "1i Code ES;Longitude;Latitude;Quartier;Ville;Contact Fix;Contact Mobile" ES_Liste_WEB_*

